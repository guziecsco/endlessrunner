﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reposition : MonoBehaviour
{
    BoxCollider2D box;
    float totalLenght;
    void Start()
    {
        box = GetComponent<BoxCollider2D>();
        totalLenght = box.size.x;
    }

    void Update()
    {
        if(transform.position.x <= -15)
        {
            transform.position = (Vector2)transform.position + Vector2.right * totalLenght * 30f; 
        }
    }
}
